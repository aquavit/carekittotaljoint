
# Summary

This is a rough refactor of the basic CareKit iOS sample application, adjusted to reflect a real world post surgery scenario related to recovery from a total hip joint replacement.

# Current State of Code

This revision retains much of the original OCK sample code. As a result it continues to stay close to the original canonical structure of a CareKit application, as per link below. 
http://carekit.org/docs/docs/Overview/Overview.html

As a deviation - the Care Plan Store is flushed each time and the HealthKit entries are similarly a works in progress. More code scrubbing and testing needed.

Instead of the activity creation being split between two methods for the medication task - a construction time init() was added. This made sense because multiple medications are needed, but incomplete as the calendar pattern would require parametrization as well.

The key views are programmatically coordinated in the Root View controller, again carrying over the approach from the original sample. The font size in those nominal views seemed a little disproportionally large. Instead of taking a direct way to dial font size down, the activity text was trimmed to be as concise as possible as a workaround for now.

![Clip](./CareKitTotalJoint.gif)

# Next Steps 

Further generalization to better parameterize, configure and create the Activities in code. Clearer distinction of the Activity and Assessment implementation approach from the original OCK sample would be part of that clean up for creation paths. Graphing data would be hooked up ask well - but again - the key is having clear quantitative metrics for the recovery context. 

## Physical Therapy Timeline

Post surgery, there are three key phases for prescribed Physical Therapy that unfold based on a patient's progress. Specifically there are exercises selected when laying down, sitting and standing - on a timeline based on the progress made post surgery. A usable CareKit app would enable the addition of new exercises and in some cases removing some from the mix. The typical mode would be addition, not necessarily removal due to the need to build on the steps preceding. 

See below for an example sets, each set building on the preceding set. 

### Phase One: Laying Down
- Elevated Angle Pumps
- Quad Set
- Supine Glut Sets
- Heel Slides
- Straight Leg Raise 
- Supine Side Leg 

### Phase Two: Sitting
- Long Arc Quad
- Hip Flexor
- Hip Abduction, Isometrics 

### Phase Three: Standing
- Hamstring Stretch
- Heel Raise
- Rubber Band

### Phase Four: Outpatient Physical Therapy
- Bridge (Adjusted)
- Top Leg Lift
- Back of Hip
- Bridge (Adjustted)
- Warrior I
- Prone Hip Extension 
- Squat

Template Upper
+ Low Row
+ Buckets

Aqautics
- Walk Patterns: forward, backward, crab walk Left and Right
- Kickboard: with or without fins

## Document and Image Storage

The OCKDocument class opens up some options for standards - reports or extracted meta data from other standards based evidence objects. In an orthopedic recovery case, with Physical Therapy in the mix, images with drawings of exercises to perform are often helpful.

## Assesments and Insights

As a more general statement - the specific metrics tracked in the Care Plan and Health Kit data stores in the area of Insights would require more thought. 

In the case of a total joint repaclement - the initial phases post surgery are more qualitative (pain) than quantitative. For a professional athlete, BMI would be a hypothetical option - but may not be typically in the mix for the general public to measure recovery.

Pain for example over time may well have very different metrics depending on both the day and the time of day as the healing and therapy progresses.

# References

## Codes: ICD-10

http://www.apta.org/ICD10/IdentifyingCodes/

Hip Arthroplasty

- Z47.1 Aftercare following joint replacement surgery
Use additional code to identify the joint (Z96.6-)

- Z96.64 Presence of artificial hip joint
Hip-joint replacement (partial) (total)

- Z96.641 Presence of right artificial hip joint 
- Z96.642 Presence of left artificial hip joint 
- Z96.643 Presence of artificial hip joint, bilateral

## Apple Frameworks: ResearchKit, CareKit, HealthKit

http://researchkit.org/docs/docs/ChartsAndGraphs/ChartsAndGraphs.html

https://developer.apple.com/library/ios/documentation/HealthKit/Reference/HKSampleQuery_Class/index.html#//apple_ref/doc/uid/TP40014753

http://carekit.org/docs/docs/Overview/Overview.html
http://carekit.org/docs/Classes/OCKCarePlanActivity.html#//api/name/identifier
http://carekit.org/docs/docs/AccessingCarePlanData/AccessingCarePlanData.html

Foundation:
https://developer.apple.com/library/ios/documentation/Cocoa/Reference/Foundation/Classes/NSFileManager_Class/#//apple_ref/doc/constant_group/File_Protection_Values
