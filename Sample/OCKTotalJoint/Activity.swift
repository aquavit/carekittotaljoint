/*
 Copyright (c) 2016, Apple Inc. All rights reserved.
 
 Redistribution and use in source and binary forms, with or without modification,
 are permitted provided that the following conditions are met:
 
 1.  Redistributions of source code must retain the above copyright notice, this
 list of conditions and the following disclaimer.
 
 2.  Redistributions in binary form must reproduce the above copyright notice,
 this list of conditions and the following disclaimer in the documentation and/or
 other materials provided with the distribution.
 
 3.  Neither the name of the copyright holder(s) nor the names of any contributors
 may be used to endorse or promote products derived from this software without
 specific prior written permission. No license is granted to the trademarks of
 the copyright holders even if such marks are included in this software.
 
 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE
 FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

import CareKit

/**
 Protocol that defines the properties and methods for sample activities.
 */
protocol Activity {
    var _activityType: ActivityType { get }
    
    func carePlanActivity( ) -> OCKCarePlanActivity
}

protocol ParameterizedActivity : Activity {
    var _activityType: ActivityType { get }
    
//    required init( activityType: ActivityType , title: String, summary: String, instructions: String )
    
    func carePlanActivity(  activityType: ActivityType, title: String, summary: String, instructions: String ) -> OCKCarePlanActivity
}

/*
 - Straight Leg Raise
 - Supine Side Leg
 *
 - Elevated Angle Pumps
 - Quad Set
 - Supine Glut Set
 - Heel Slide
 */

// Enumeration of strings used as identifiers for the prescribed Activity set
enum ActivityType: String {
    
    //Activity set
    case OutdoorWalk
    case StraightLegRaise
    case SupineSideLeg
    
    case ElevatedAnglePumps
    case QuadSet
    case SupineGlutSet
    case HeelSlide
    
    case TakeMedication
    case TakeMedicationOriginal
    
    case SittingExercise
    case StandingExercise
    
    //Assesments
    case Mood
    case Weight
    case JointPain
    case IncisionSite
    
}
