/*
 Copyright (c) 2016, Apple Inc. All rights reserved.
 
 Redistribution and use in source and binary forms, with or without modification,
 are permitted provided that the following conditions are met:
 
 1.  Redistributions of source code must retain the above copyright notice, this
 list of conditions and the following disclaimer.
 
 2.  Redistributions in binary form must reproduce the above copyright notice,
 this list of conditions and the following disclaimer in the documentation and/or
 other materials provided with the distribution.
 
 3.  Neither the name of the copyright holder(s) nor the names of any contributors
 may be used to endorse or promote products derived from this software without
 specific prior written permission. No license is granted to the trademarks of
 the copyright holders even if such marks are included in this software.
 
 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE
 FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

import ResearchKit
import CareKit

class SampleData: NSObject {
    
    // MARK: Properties

    /// An array of `Activity`s used in the app.
    let activities: [Activity] = [
        TakeMedication( activityType: ActivityType.TakeMedication, title: "Aspirin", summary: "81mg", instructions: "Take with food.",  startDate: NSDateComponents(year: 2016, month: 05, day: 07), eachDayPattern: [3, 3, 3, 3, 3, 3, 3] ),
        TakeMedication( activityType: ActivityType.TakeMedication, title: "Percocet", summary: "325mg", instructions: "Take with food. No more than 4x day, every 6 hours - use pain as guide.",  startDate: NSDateComponents(year: 2016, month: 05, day: 07), eachDayPattern: [3, 3, 3, 3, 3, 3, 3]),
        TakeMedication( activityType: ActivityType.TakeMedication, title: "Laxative", summary: "Pill or powder", instructions: "Take while on Percocet.", startDate: NSDateComponents(year: 2016, month: 05, day: 07), eachDayPattern: [3, 3, 3, 3, 3, 3, 3] ),

        StraightLegRaise(),
        SupineSideLeg(),
        ElevatedAnglePumps(),
        QuadSet(),
        SupineGlutSet(),
        HeelSlide(),
        
        SittingExercise( activityType: ActivityType.SittingExercise, title: "Hip Flexor", summary: "1 set, 10 reps, 3x day", instructions: "-While seated, lift leg off ground at knee\n=Repeat.", startDate: NSDateComponents(year: 2016, month: 05, day: 13), eachDayPattern: [3, 3, 3, 3, 3, 3, 3] ),
        SittingExercise( activityType: ActivityType.SittingExercise, title: "Long Arc Quad", summary: "1 set, 10 reps, 3x day", instructions: "-While seated, kick leg out at knee\n=Repeat.", startDate: NSDateComponents(year: 2016, month: 05, day: 13), eachDayPattern: [3, 3, 3, 3, 3, 3, 3] ),
        SittingExercise( activityType: ActivityType.SittingExercise, title: "Hip Abduction", summary: "1 set, 10 reps, 3x day", instructions: "-While seated, press legs together with pillow in between\n-Push leg out against pressure from hand\n=Repeat.", startDate: NSDateComponents(year: 2016, month: 05, day: 13), eachDayPattern: [3, 3, 3, 3, 3, 3, 3] ),
        
        StandingExercise( activityType: ActivityType.StandingExercise, title: "Heel Raise", summary: "1 set, 10 reps, 3x day",
            instructions: "-Stand with arms out against surface for balance (e.g. a counter).\n-Raise heels off the ground keeping ball of feet in place.\n-Repeat.",
            startDate: NSDateComponents(year: 2016, month: 05, day: 17), eachDayPattern: [3, 3, 3, 3, 3, 3, 3] ),
        StandingExercise( activityType: ActivityType.StandingExercise, title: "Hamstring Stretch", summary: "1 set, 10 reps, 3x day",
            instructions: "-Stand with arms out against surface ffor balance (e.g. a counter).\n-Keeping leg roughtly straight and out- ease down to stretch hamstring.\n-Alternate legs and Repeat.",
            startDate: NSDateComponents(year: 2016, month: 05, day: 17), eachDayPattern: [3, 3, 3, 3, 3, 3, 3] ),
        StandingExercise( activityType: ActivityType.StandingExercise, title: "Rubber Band", summary: "1 set, 10 reps, 3x day",
            instructions: "-Stand with  one leg in band secured in door at shin level\n-Face door and kick leg out backwards with tension.\nTurn around and, facing away, move leg out to point of tension.\n-Alternate legs and Repeat.",
            startDate: NSDateComponents(year: 2016, month: 05, day: 17), eachDayPattern: [3, 3, 3, 3, 3, 3, 3] ),
        //LATER ---> OutdoorWalk()
        
        JointPain(),
        IncisionSite(),
        Mood(),
        Weight()

    ]
    
    /**
        An array of `OCKContact`s to display on the Connect view.
    */
    let contacts: [OCKContact] = [
        OCKContact(contactType: .CareTeam,
            name: "Primary Care, MD",
            relation: "Physician",
            tintColor: Colors.Blue.color,
            phoneNumber: CNPhoneNumber(stringValue: "888-555-5512"),
            messageNumber: CNPhoneNumber(stringValue: "888-555-5512"),
            emailAddress: "PrimaryCarePhysican@kp.org",
            monogram: "PC",
            image: nil),
        
        OCKContact(contactType: .CareTeam,
            name: "Orthopedic Surgeon, MD",
            relation: "Surgeon",
            tintColor: Colors.Green.color,
            phoneNumber: CNPhoneNumber(stringValue: "888-555-5512"),
            messageNumber: CNPhoneNumber(stringValue: "888-555-5512"),
            emailAddress: "OrthopedicSurgeon@kp.org",
            monogram: "OS",
            image: nil),
        
        OCKContact(contactType: .CareTeam,
            name: "Physical Therapist, PT",
            relation: "PhysicalTherapist",
            tintColor: Colors.Red.color,
            phoneNumber: CNPhoneNumber(stringValue: "888-555-5512"),
            messageNumber: CNPhoneNumber(stringValue: "888-555-5512"),
            emailAddress: "PhysicalTherapist@kp.org",
            monogram: "PT",
            image: nil),
        
        OCKContact(contactType: .Personal,
            name: "Family Member",
            relation: "Family",
            tintColor: Colors.Yellow.color,
            phoneNumber: CNPhoneNumber(stringValue: "888-555-5512"),
            messageNumber: CNPhoneNumber(stringValue: "888-555-5512"),
            emailAddress: nil,
            monogram: "FM",
            image: nil)
    ]
    
    // MARK: Initialization
    
    required init(carePlanStore: OCKCarePlanStore) {
        super.init()

        // Populate the store with the sample activities.
        for sampleActivity in activities {
            let carePlanActivity = sampleActivity.carePlanActivity()
            
            carePlanStore.addActivity(carePlanActivity) { success, error in
                if !success {
                    print(error?.localizedDescription)
                }
            }
        }
        
        let test = activityWithType(ActivityType.StraightLegRaise)
        
        
    }
    
    // MARK: Convenience
    
    /// Returns the `Activity` that matches the supplied `ActivityType`.
    func activityWithType(type: ActivityType) -> Activity? {
        for activity in activities where activity._activityType == type {
            return activity
        }
        
        return nil
    }
    
    func generateSampleDocument() -> OCKDocument {
        let subtitle = OCKDocumentElementSubtitle(subtitle: "First subtitle")
        
        let paragraph = OCKDocumentElementParagraph(content: "Lorem ipsum dolor sit amet, vim primis noster sententiae ne, et albucius apeirian accusata mea, vim at dicunt laoreet. Eu probo omnes inimicus ius, duo at veritus alienum. Nostrud facilisi id pro. Putant oporteat id eos. Admodum antiopam mel in, at per everti quaeque. Lorem ipsum dolor sit amet, vim primis noster sententiae ne, et albucius apeirian accusata mea, vim at dicunt laoreet. Eu probo omnes inimicus ius, duo at veritus alienum. Nostrud facilisi id pro. Putant oporteat id eos. Admodum antiopam mel in, at per everti quaeque. Lorem ipsum dolor sit amet, vim primis noster sententiae ne, et albucius apeirian accusata mea, vim at dicunt laoreet. Eu probo omnes inimicus ius, duo at veritus alienum. Nostrud facilisi id pro. Putant oporteat id eos. Admodum antiopam mel in, at per everti quaeque.")
            
        let document = OCKDocument(title: "Sample Document Title", elements: [subtitle, paragraph])
        document.pageHeader = "App Name: OCKTotalJoint, User Name: John Appleseed"
        
        return document
    }
}
