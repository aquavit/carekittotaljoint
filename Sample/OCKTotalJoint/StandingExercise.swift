//
//  StandingExercise.swift
//  OCKTotalJoint
//
//  Created by John Matthew Weston on 5/20/16.
//  Copyright © 2016 Apple. All rights reserved.
//

import CareKit

struct StandingExercise : Activity {
    // MARK: Activity
    
    var _activityType: ActivityType
    
    var _activity: OCKCarePlanActivity
    
    init( activityType: ActivityType, title: String, summary: String, instructions: String, startDate: NSDateComponents, eachDayPattern: [NSNumber] )
    {
        _activityType = activityType
        
        // Create a weekly schedule.
        let startDate = startDate
        let schedule = OCKCareSchedule.weeklyScheduleWithStartDate(startDate, occurrencesOnEachDay: eachDayPattern )
        
        // Get the localized strings to use for the activity.
        let title = NSLocalizedString( title, comment: "")
        let summary = NSLocalizedString( summary, comment: "")
        let instructions = NSLocalizedString( instructions, comment: "")
        
        let key = _activityType.rawValue+"_"+title+"_"+summary
        
        _activity = OCKCarePlanActivity.interventionWithIdentifier(
            key,
            groupIdentifier: nil,
            title: title,
            text: summary,
            tintColor: Colors.Green.color,
            instructions: instructions,
            imageURL: nil,
            schedule: schedule,
            userInfo: nil
        )
        
    }
    
    mutating func setPlanActivity( activity: OCKCarePlanActivity )
    {
        _activity = activity
    }
    
    // -due to combined SampleData collection of Activity's
    // -supports Activity protocol
    func carePlanActivity() -> OCKCarePlanActivity {
        return _activity
    }
}