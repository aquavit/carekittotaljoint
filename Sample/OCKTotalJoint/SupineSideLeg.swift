//
//  SupineSideLeg.swift
//  OCKTotalJoint
//
//  Created by John Matthew Weston on 5/9/16.
//  Source Code - Copyright © 2016 John Matthew Weston but published as open source under MIT License.
//

import CareKit

struct SupineSideLeg: Activity {
    // MARK: Activity
    
    let _activityType: ActivityType = .SupineSideLeg
    
    func carePlanActivity() -> OCKCarePlanActivity {
        // Create a weekly schedule.
        let startDate = NSDateComponents(year: 2016, month: 05, day: 07)
        let schedule = OCKCareSchedule.weeklyScheduleWithStartDate(startDate, occurrencesOnEachDay: [3, 3, 3, 3, 3, 3, 3])
        
        // Get the localized strings to use for the activity.
        let title = NSLocalizedString("Supine Side Leg", comment: "")
        let summary = NSLocalizedString("1 set, 10 reps, 3x day", comment: "")
        let instructions = NSLocalizedString("-Lie back, firm surface, legs together.\n-Move leg out to side, knee straight.\n-Return to starting position.", comment: "")
        
        // Create the intervention activity.
        let activity = OCKCarePlanActivity.interventionWithIdentifier(
            _activityType.rawValue,
            groupIdentifier: nil,
            title: title,
            text: summary,
            tintColor: Colors.Blue.color,
            instructions: instructions,
            imageURL: nil,
            schedule: schedule,
            userInfo: nil
        )
        
        return activity
    }
}





