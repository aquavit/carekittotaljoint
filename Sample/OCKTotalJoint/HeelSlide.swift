//
//  HeelSlide.swift
//  OCKTotalJoint
//
//  Created by John Matthew Weston on 5/10/16.
//  Source Code - Copyright © 2016 John Matthew Weston but published as open source under MIT License.
//

import CareKit

struct HeelSlide: Activity {
    // MARK: Activity
    
    let _activityType: ActivityType = .HeelSlide
    
    func carePlanActivity() -> OCKCarePlanActivity {
        // Create a weekly schedule.
        let startDate = NSDateComponents(year: 2016, month: 05, day: 07)
        let schedule = OCKCareSchedule.weeklyScheduleWithStartDate(startDate, occurrencesOnEachDay: [3, 3, 3, 3, 3, 3, 3])
        
        // Get the localized strings to use for the activity.
        let title = NSLocalizedString("Heel Slide", comment: "")
        let summary = NSLocalizedString("1 set, 10 reps, 3x day", comment: "")
        let instructions = NSLocalizedString("-Lie on back with legs straight.\n-Slide heel up to buttocks.\n-Return to start position.", comment: "")
        
        // Create the intervention activity.
        let activity = OCKCarePlanActivity.interventionWithIdentifier(
            _activityType.rawValue,
            groupIdentifier: nil,
            title: title,
            text: summary,
            tintColor: Colors.Blue.color,
            instructions: instructions,
            imageURL: nil,
            schedule: schedule,
            userInfo: nil
        )
        
        return activity
    }
}
