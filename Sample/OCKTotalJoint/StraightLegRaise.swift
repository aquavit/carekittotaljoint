//
//  StraightLegRaise.swift
//  OCKTotalJoint
//
//  Created by John Matthew Weston on 5/9/16.
//  Source Code - Copyright © 2016 John Matthew Weston but published as open source under MIT License.
//

import CareKit

/*
 - Struct that conforms to the `Activity` protocol
 -  Refactored from the original struct that defined a hamstring stretch activity.
 -
 - Straight Leg Raise
 - Supine Side Leg
 *
 - Elevated Angle Pumps
 - Quad Set
 - Supine Glut Set
 - Heel Slide
 */

struct StraightLegRaise: Activity {
    // MARK: Activity
    
    let _activityType: ActivityType = .StraightLegRaise
    
    func carePlanActivity() -> OCKCarePlanActivity {
        // Create a weekly schedule.
        let startDate = NSDateComponents(year: 2016, month: 05, day: 07)
        let schedule = OCKCareSchedule.weeklyScheduleWithStartDate(startDate, occurrencesOnEachDay: [3, 3, 3, 3, 3, 3, 3])
        
        // Get the localized strings to use for the activity.
        let title = NSLocalizedString("Straight Leg Raise", comment: "")
        let summary = NSLocalizedString("1 set, 10 reps, 3x day", comment: "")
        let instructions = NSLocalizedString("-Lie on back with opposite knee bent.\n-Raise leg to thigh level of bent leg.\n-Return to starting position.", comment: "")
        
        // Create the intervention activity.
        let activity = OCKCarePlanActivity.interventionWithIdentifier(
            _activityType.rawValue,
            groupIdentifier: nil,
            title: title,
            text: summary,
            tintColor: Colors.Blue.color,
            instructions: instructions,
            imageURL: nil,
            schedule: schedule,
            userInfo: nil
        )
        
        return activity
    }
}
